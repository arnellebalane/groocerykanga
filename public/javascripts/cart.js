const auth = firebase.auth();
const firestore = firebase.firestore();
const storage = firebase.storage();

auth.onAuthStateChanged(user => {
    if (user) {
        updateInterface();
        initializePage();
    } else {
        window.location.pathname = '/';
    }
});

function updateInterface() {
    const user = auth.currentUser;

    $('.user-avatar').attr('src', user.photoURL || 'images/default-avatar.png');
    $('.user-avatar').attr('alt', user.displayName);
    $('.user-name').text(user.displayName);
}

async function initializePage() {
    const orders = await firestore.collection('orders');
    const currentOrderId = localStorage.getItem('current-order');
    // const currentOrder = currentOrderId
    //     ? orders.doc(currentOrderId)
    //     : ();
    const currentOrder = await orders.add({
        totalPrice: 0,
        delivered: false,
        createdAt: firebase.firestore.FieldValue.serverTimestamp()
    });
    localStorage.setItem('current-order', currentOrder.id);

    const currentOrderItems = currentOrder.collection('items');
    const cartItemTemplate = Handlebars.compile($('template#item').html());
    const cartItemsContainer = $('.cart-items');

    $('.logout-btn').on('click', e => {
        auth.signOut();
    });

    $('.cart-form').on('submit', async e => {
        e.preventDefault();
        var item = {
            name: e.target.item.value,
            price: parseFloat(e.target.price.value, 10),
            quantity: parseInt(e.target.quantity.value, 10)
        };
        if (e.target.image.files.length > 0) {
            const uploadRef = storage.ref(`images/${generateRandomKey()}`);
            const uploadSnapshot = await uploadRef.put(e.target.image.files[0]);
            item.image = uploadSnapshot.downloadURL;
        }
        e.target.reset();
        addItemToCart(item);
    });

    $('.cart-items').on('change', '.item-quantity', e => {
        const id = $(e.target).closest('.cart-item').data('id');
        const quantity = parseInt(e.target.value, 10);
        currentOrderItems.doc(id).update({ quantity });
    });

    currentOrder.onSnapshot(snapshot => {
        console.log(snapshot);
        const data = snapshot.data();
        const totalPrice = data.totalPrice.toLocaleString('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });
        $('.total-price').text(totalPrice);
    });

    currentOrderItems.orderBy('createdAt').onSnapshot(snapshot => {
        snapshot.docChanges.forEach(docChange => {
            const doc = docChange.doc;
            if (docChange.type === 'added') {
                const rendered = renderCartItem(doc.id, doc.data());
                cartItemsContainer.append(rendered);
            } else if (docChange.type === 'modified') {
                const rendered = renderCartItem(doc.id, doc.data());
                cartItemsContainer.find(`[data-id="${doc.id}"]`).replaceWith(rendered);
            }
        });
    });

    function addItemToCart(item) {
        item.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return currentOrderItems.add(item);
    }

    function renderCartItem(id, item) {
        item.id = id;
        if (item.totalPrice) {
            item.totalPrice = item.totalPrice.toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
        }
        return $(cartItemTemplate(item));
    }

    function generateRandomKey() {
        return Math.random().toString(16).substring(2);
    }
}
