const auth = firebase.auth();

auth.onAuthStateChanged(function(user) {
    if (user) {
        var userCollection = firebase.firestore().collection("users");

        userCollection.doc(user.uid).get().then(async function(data) {
            if (!data.exists) {
                var userData = {
                    displayName: user.displayName,
                    email: user.email,
                    phoneNumber: user.phoneNumber,
                    photoURL: user.photoURL
                };
                await userCollection.doc(user.uid).set(userData);
                window.location.href = "/user-type.html";
            } else {
                const type = data.data().type;
                if (type === 'Customer') {
                    await userCollection.doc(user.uid).update({
                        type: 'Customer'
                    });
                    window.location.href = "/cart.html";
                } else if (type === 'Rider') {
                    await userCollection.doc(user.uid).update({
                        type: 'Rider'
                    });
                    window.location.href = "/rider-settings.html";
                }
                window.location.href = "/cart.html";
            }
        });
    } else {
        console.log("null");
    }
});

function signInWithGoogle() {
    var provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
}

function signInWithFacebook() {
    var provider = new firebase.auth.FacebookAuthProvider();
    auth.signInWithPopup(provider);
    auth.onAuthStateChanged(function(user) {
        if (user != null) {
            console.log("hi");
            var userCollection = firebase.firestore().collection("users");
            userCollection.doc(user.uid).get().then(function(data) {
                    console.log(data.data());
                    if (data.data().type === null) {
                        window.location.href = "/user-type.html";
                        if ($("input[name=user]:checked").val() === 'Customer') {
                            data.data().type = 'Customer';
                            window.location.href = "/cart.html";
                        } else if ($("input[name=user]:checked").val() === 'Rider') {
                            data.data().type = 'Rider';
                            window.location.href = "/rider-settings.html";
                        }
                        var userData = {
                            displayName: user.displayName,
                            email: user.email,
                            phoneNumber: user.phoneNumber,
                            photoURL: user.photoURL,
                            type: data.data().type
                        };
                        userCollection.doc(user.uid).set(userData).then(function() {
                            console.log(userData);
                        });
                    }
            });
        } else {
            console.log("null");
        }
    });
}

function signInWithTwitter() {
    var provider = new firebase.auth.TwitterAuthProvider();
    auth.signInWithPopup(provider);
    auth.onAuthStateChanged(function(user) {
        console.log(user);
        if (user != null) {
            console.log("hi");
            var userCollection = firebase.firestore().collection("users");
            userCollection.doc(user.uid).get().then(function(data) {
                    console.log(data.data());
                    if (data.data().type === null) {
                        window.location.href = "/user-type.html";
                        if ($("input[name=user]:checked").val() === 'Customer') {
                            data.data().type = 'Customer';
                            window.location.href = "/cart.html";
                        } else if ($("input[name=user]:checked").val() === 'Rider') {
                            data.data().type = 'Rider';
                            window.location.href = "/rider-settings.html";
                        }
                        var userData = {
                            displayName: user.displayName,
                            email: user.email,
                            phoneNumber: user.phoneNumber,
                            photoURL: user.photoURL,
                            type: data.data().type
                        };
                        userCollection.doc(user.uid).set(userData).then(function() {
                            console.log(userData);
                        });
                    } else {
                        console.log("hey");
                    }
            });
        } else {
            console.log("null");
        }
    });
}

function signOut() {
    auth.signOut().then(function() {
        console.log('Signed Out');
    }, function(error) {
        console.error('Sign Out Error', error);
    });
}

function redirect() {
    var buttonVal = $("input[name=user]:checked").val();
    if (buttonVal === 'Customer') {
        window.location.href = "/cart.html";
    } else {
        window.location.href = "/rider-settings.html";
    }
}
