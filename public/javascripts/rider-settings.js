(async () => {

	const firestore = firebase.firestore();
	const auth = firebase.auth();

	auth.onAuthStateChanged(user => {
	    if (user) {
	        currUser();

            firestore.collection('users').doc(user.uid).get().then(snapshot => {
                const data = snapshot.data();
                $('.active-toggle').prop('checked', data.active);
            })
	    } else {
	        window.location.pathname = '/';
	    }
	});

    $('.logout-btn').on('click', e => {
        auth.signOut();
    });

	$('.rider-settings').on('submit', e => {
        e.preventDefault();
        var user = {
            displayName: e.target.displayName.value,
            email: e.target.email.value
        };
        editUser(user);
    });

	$(".active-toggle").change(function() {
		var user = firebase.auth().currentUser;
        if($(this).prop('checked') == true) {
            console.log("Online");
            firestore.collection('users').doc(user.uid).update({
				active: true
			});
        } else {
            console.log("Offline");
            firestore.collection('users').doc(user.uid).update({
				active: false
			});
        }
    });

    function currUser(user) {
    	var user = firebase.auth().currentUser;

        $('.user-avatar').attr('src', user.photoURL || 'images/default-avatar.png');
        $('.user-avatar').attr('alt', user.displayName);
        $('.user-name').text(user.displayName);

        $('form .display-name').val(user.displayName);
        $('form .email').val(user.email);
    }

    async function editUser(updates) {
        var user = firebase.auth().currentUser;

		await Promise.all([
			user.updateProfile({
			  	displayName: updates.displayName,
				photoURL: updates.photoURL
			}),
			user.updateEmail(updates.email).then(function() {
			  	console.log("Email updated successfully!")
			})
		])

		await firestore.collection('users').doc(user.uid).update({
			displayName: user.displayName,
			email: user.email,
			photoURL: user.photoURL,
			phoneNumber: user.phoneNumber
		});
		currUser(user);

    }




})();
