const functions = require('firebase-functions');

exports.computeItemTotalPrice = functions.firestore.document('orders/{orderId}/items/{itemId}').onWrite(e => {
    if (!e.data.data()) return;

    const item = e.data.data();
    const totalPrice = item.price * item.quantity;
    return e.data.ref.update({ totalPrice }).then(() => new Promise((resolve, reject) => {
        e.data.ref.parent.parent.collection('items').onSnapshot(snapshot => {
            const orderTotalPrice = snapshot.docs.reduce((total, doc) => {
                doc = doc.data();
                return doc.totalPrice ? total + doc.totalPrice : total;
            }, 0);
            e.data.ref.parent.parent.update({ totalPrice: orderTotalPrice }).then(resolve, reject);
        });
    }));
});
